var http = require('http');
var Iconv = require('iconv').Iconv
var parseXML = require('xml2js').parseString;

module.exports.getAllTransgressor = function(callback) {
    var req = http.get(config.fetcher.host, function (response) {
        var bufferChunk = [];
        
        if(response.statusCode == 200 || response.statusCode == 304) {
        
            response.on('data', function(chunk) {
                bufferChunk.push(chunk);
            })
            
            response.on('end', function() {
                var iconv = Iconv('latin1', 'UTF-8');
                var str = iconv.convert(Buffer.concat(bufferChunk)).toString();

                parseXML(str, function(err, result) {
                    if(err) {
                        winston.error("Could not parse string.");
                        winston.error(str);
                        throw err;
                    }
                    callback(null, result);
                });

            });

        } else {
            winston.error("Fetching response is invalid. Status code wasn't not 200 nor 304.");
            callback('Ressource returned unexpected status code (fetch_controller.js)', null)
        }
    });

    req.on('error', function(err) {
        winston.error("Fetching request isn't did not work.");
        winston.error(req);
        callback(err, null);
    });
}
