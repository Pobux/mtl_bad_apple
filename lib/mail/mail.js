var nodemailer = require("nodemailer");
var EmailTemplate = require("email-templates").EmailTemplate;
var path = require("path");
var moment = require("moment");

module.exports.sendUpsertUpdateTo = function(receiver, results) {
    winston.info("Sending e-mail");
    winston.info(receiver);

    var templateDir = path.join(__dirname, "templates", "upsert");
    var upsertLetter = new EmailTemplate(templateDir);

    results = results.slice(0, config.mail.max_per_mail);
    results = _beautifyDateFormatOfResults(results);

    var transporter = _createTransporter()
    var mailOptions = _createMailOptions()
    var hasMultipleResults = results.length > 1;

    upsertLetter.render({
        transgressors:results,
        multiple: hasMultipleResults 
    }, function(err, result) {
        if(err) {
            winston.error("Couldn't render template.");
            throw err;
        }

        mailOptions.text = result.text;
        mailOptions.html = result.html;

        transporter.sendMail(mailOptions, function(err, info) {
           if(err) {
               winston.error("error", "Could not send mail\n " + info)
           }
        })
    })
}

function _beautifyDateFormatOfResults(results) {
    var result;
    moment.locale("fr");

    for (var i = 0, len = results.length; i < len; i++) {
        result = results[i];
        result.infraction_date = moment(result.infraction_date).format("DD MMM YYYY");
        result.judgement_date = moment(result.judgement_date).format("DD MMM YYYY");
        results[i] = result;
    }

    return results;
}

function _createTransporter() {
    var transporter = nodemailer.createTransport({
        service: config.mail.service,
        auth: {
            user: config.mail.user,
            pass: config.mail.pass 
        }
    })

    return transporter;
}

function _createMailOptions() {
    var mailOptions = {
        from: config.mail.from,
        to: config.mail.to,
        subject: config.mail.upserted_subject,
    }

    return mailOptions;
}
