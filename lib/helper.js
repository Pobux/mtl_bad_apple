module.exports.isElementLastOf = function(element, list) {
    return list[list.length - 1] === element;
}

module.exports.isMongoHasNewInsertion = function(result) {
/*
    base on mongodb pattern
        {ok: /, nModified: 0, n:1} 
    vs
        { ok: 1,
        nModified: 0,
        n: 1,
        upserted: [ { index: 0, _id: 57f7c5cc22031b4b92751ae3 } ] }
*/
    return result["upserted"] != undefined;
}

module.exports.filterObjectListByKey = function(objectList, key) {
    /*
       This reconfigure a dictionnary based on a given key.
       This key will be a top level to the new dict.
    */
    var newObjectList = [];
    var distinctNameList = [];
    var value;

    for (var i = 0, len = objectList.length; i < len; i++) {
        value = objectList[i][key]; 

        if(distinctNameList.indexOf( value ) == -1) {
            distinctNameList.push( value );
            newObjectList.push(objectList[i]);
        }
    }

    return newObjectList;
}
