var moment = require("moment");
var transgressorModel = require("./transgressor");
var transgressorFormatter = require("./transgressor_formatter");
var help = require("../helper.js");
var ObjectId = require("mongoose").Types.ObjectId; 

module.exports.bulkInsertTransgressor = function(transgressorList) {
    transgressorList.forEach(function(transgressor) {
        transgressorModel.addNewTransgressor(transgressor);
    });
}

module.exports.bulkUpsertTransgressor = function(transgressorList, callback) {
    //Return a list of newly distinct added objects
    var newTransgressorList = [];

    transgressorList.forEach(function(transgressor) {
        transgressorModel.upsertTransgressor(transgressor, function(err, result) {
            if(err) {
                winston.error("Couldn't insert new transgressor.");
                winston.error(transgressor);
            } else {
                if(help.isMongoHasNewInsertion(result)) {
                    newTransgressorList.push(result);
                }
                
                if(help.isElementLastOf(transgressor, transgressorList) &&
                        newTransgressorList.length > 0) {
                    //Looping the transgressor list is done and there's new transgressors.
                    //Unfortunatly, upsert only returns _id, program needs to get a list 
                    //of complete distinct objects
                    _getNewDistinctUpsertedObjectList(newTransgressorList, callback);
                }
            }
        });
    });
}

function _getNewDistinctUpsertedObjectList(newTransgressorList, callback) {
    //distinct objects are needed as per client request
    transgressorModel.findAllFromUpsertPattern(newTransgressorList, function(err, results) {
        if(!results || err) {
            callback("Not found.", null)
        } else {
            results = help.filterObjectListByKey(results, "businessName"); 
            callback(null, results);
        }
    });
}

module.exports.dateInterval = function(from, to, callback) {
    if(_isValidDate(from) && _isValidDate(to)) {
        transgressorModel.findAll({
            "infraction_date" : {$gte: from, $lte: to}
        }, callback);
    } else {
        callback("Not found.", null);
    }
}

function _isValidDate(date) {
    return moment(date, "YYYY-MM-DD", true).isValid();    
}

module.exports.businessNameList = function(callback) {
    transgressorModel.findAllDistinctBusinessName(callback);
}

module.exports.findTransgressorById = function(id, callback) {
    var error = false;

    try {
        var query = {"_id" : new ObjectId(id)}
    } catch(e) {
        callback("Wrong format.", null);
        error = true;
    }

    if(!error) {
        transgressorModel.findOne(query, callback);
    }
}

module.exports.findTransgressorByName = function(name, callback) {
    var name = name.toUpperCase();
    var query = {"businessName" : name}

    transgressorModel.findAll(query, function(err, results) {
        if(err || !results || results.length == 0) {
            callback("Not found.", null)
        } else {
            transgressorFormatter.oneBusinessNameMultipleInfraction(results, callback);
        }
    });
}

module.exports.findTransgressorByInfractionsCount = function(callback) {
    //Formatter takes care of summation since it has to restructure the output
    var selectString = "businessName"
    transgressorModel.findAllBySelect(selectString, function(err, results) {

        if(err) {
            winston.error("Problem finding by selection (" + selectString + ")" );
            callback("Can't find all by infraction", null)
        }

        transgressorFormatter.infractionSum(results, callback);
    }); 
}
