/*
   Edit results
   Use formatter between database results and controller
   to beautitfy, delete or add keys/values
*/

module.exports.oneBusinessNameMultipleInfraction = function(results, callback) {
    //BusinessName would not change, it is assume that it's the same for the following
    //keys
    if(!results) callback("Not found", null);

    var newResults = {
        "businessName" : results[0]["businessName"],
        "owner" : results[0]["owner"],
        "address" : results[0]["address"],
        "city" : results[0]["city"],
        "category" : results[0]["category"],
        "infractions" : [] 
    }

    for (var i = 0, len = results.length; i < len; i++) {
        newResults["infractions"].push({
            "transgressor_id" : results[i]["_id"],
            "description" : results[i]["description"],
            "infraction_date" : results[i]["infraction_date"],
            "judgement_date" : results[i]["judgement_date"],
            "amount" : results[i]["amount"]
        });
    }

    callback(null, newResults);
}

module.exports.infractionSum = function(results, callback) {
    var newResults = [];
    var distinctNameList = [];
    var result;
    
    for (var i = 0, len = results.length; i < len; i++) {
        result = results[i]; 

        if(distinctNameList.indexOf(result["businessName"]) == -1) {
            distinctNameList.push(result["businessName"]);
            newResults.push({
                businessName : result["businessName"],
                infractionCount : 1
            });
        } else {
            newResults = _incrementInfractionByOne(newResults, result["businessName"]);
            newResults = _sortByInfractionCount(newResults);
        }
    }

    callback(null, newResults); 
}

function _incrementInfractionByOne(businessList, businessName) {
    for(var i in businessList) {
        if(businessList[i]["businessName"] == businessName) {
            businessList[i]["infractionCount"]++;
            break;
        }
    }
    
    return businessList;
}

function _sortByInfractionCount(newResults) {
    return newResults.sort(function(a,b) {
        return b["infractionCount"] - a["infractionCount"];
    });
}
