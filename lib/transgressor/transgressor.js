var mongoose = require("mongoose");
var help = require("../helper.js");
var ObjectId = require("mongoose").Types.ObjectId; 
var moment = require("moment");
var res = require("request");

var host = config.database.host;
var port = config.database.port;
var user = config.database.user;
var pass = config.database.pass;
var db = config.database.dbname;
var dbHost = "mongodb://" + user + ":" + pass + "@" + host + ":" + port + "/" + db 

mongoose.connect(dbHost, function(err) {
    if (err) {
        winston.error("Database connection issue. Couldn't not connect! : " + dbHost);
        throw err;
    }
});

mongoose.connection.on("disconnected", function() {
    winston.error("Database connection issue. Disconnected from : " + dbHost);
    throw new Error;
});

var transgressorSchema = mongoose.Schema({
    owner:String,
    category:String,
    businessName:String,
    address:String,
    city:String,
    description:String,
    infraction_date:Date,
    judgement_date:Date,
    amount:String
});

var TransgressorModel = mongoose.model("Transgressor", transgressorSchema);

module.exports.findOne = function(query, callback) {
    TransgressorModel.findOne(query, function(err, docs) {
        if(err || docs.length == 0)  {
            callback("Not found.", null)
        } else {
            callback(null, docs);
        }
    });
}

module.exports.findAll = function(query, callback) {
    TransgressorModel.find(query, function(err, docs) {
        if(err || docs.length == 0) {
            callback("Not found.", null)
        } else {
            callback(null, docs);
        } 
    });
}

module.exports.findAllDistinctBusinessName = function(callback) {
    TransgressorModel.
        find({}).
        select({"businessName" : 1, "_id" : 1}).
        sort({"businessName" : 1}).
        exec(function(err, results) {
            //distinct cannot be used with sort.
            results = help.filterObjectListByKey(results, "businessName");
            callback(err, results);
        });
}

module.exports.findAllBySelect = function(selectString, callback) {
    TransgressorModel.find({}, selectString, callback)
}

module.exports.addNewTransgressor = function(body) {
    if(!_isValidTransgressor(body)) {
        winston.error("Upserting did not work properly. Transgressor is invalid.");
        throw new Error("Wrong input, transgressor is invalid.");
    }

    var transgressor = _createTransgressor(body)

    transgressor.save(function(err, result) {
        var errMsg = "Could not save to database."
        winston.error(errMsg);
        if(err) throw Error(errMsg);
    });
}

function _createTransgressor(body) {
    return new Transgressor(_buildObjectAttributes(body))
}

module.exports.upsertTransgressor = function(body, callback) {
    if(!_isValidTransgressor(body)) {
        winston.error("Upserting did not work properly. Transgressor is invalid.");
        callback("Wrong input, transgressor is invalid.", null);
    }

    var transgressorQuery = _buildObjectAttributes(body);

    TransgressorModel.update(
        transgressorQuery,
        {$setOnInsert: transgressorQuery},
        {upsert: true},
        function(err, result) {
            callback(err, result);
        }
    );
}
function _isValidTransgressor(body) {
    if(
        body.owner == "" ||
        body.category == "" ||
        body.businessName == "" ||
        body.address == "" ||
        body.city == "" ||
        body.description == "" ||
        body.infraction_date == "" ||
        body.judgement_date == "" ||
        body.amount == ""
      ) {
        return false;
    }
    return true; 
}

function _buildObjectAttributes(body) {
    return {
        owner: body.proprietaire,
        category: body.categorie,
        businessName: body.etablissement,
        address: body.adresse,
        city: body.ville,
        description: body.description,
        infraction_date: _dateToISO(body.date_infraction),
        judgement_date: _dateToISO(body.date_jugement),
        amount: body.montant
    }
}

function _dateToISO(date) {
    return moment(date[0], "DD MMM YYYY", "fr").format();
}

module.exports.findAllFromUpsertPattern = function(transgressorList, callback) {
    TransgressorModel.find({
        "_id": { $in: _listObjectId(transgressorList) } 
    }, function(err, docs) {

        if(docs.length == 0 || !docs) {
            var err = "Not found.";
        } else {
            callback(err, docs)
        }
    });
}

function _listObjectId(transgressorList) {
    var objectIdList = [];
    
    transgressorList.forEach(function(transgressor) {
        objectIdList.push(
            new ObjectId(transgressor["upserted"][0]["_id"])
        ); 
    });
    
    return objectIdList;
}
