var Twitter = require("twitter");

var client = new Twitter({
    consumer_key: config.twitter.consumer_key,
    consumer_secret: config.twitter.consumer_secret,
    access_token_key: config.twitter.access_token_key,
    access_token_secret: config.twitter.access_token_secret
});

module.exports.tweetNewTransgressors = function(transgressors) {
    var len = transgressors.length;

    for (var i = 0; i < len ; i++) {
        setTimeout( function (i) {
            client.post('statuses/update', 
                { status: _buildTweetMsg(transgressors[i]) },
                function(err, tweet, response) {
                    if(err) {
                        winston.error("Tweeter could not send msg :\n" + JSON.stringify(response));
                    } else {
                        winston.info("Tweet has been sent. : " + tweet);
                    }
                }
            );
        }, config.twitter.twitter_interval * i, i);
    }
}

function _buildTweetMsg(transgressor) {
    var msg = transgressor.businessName + 
              " doit payer " +
              transgressor.amount +
              " / " +
              transgressor.businessName +
              " must pay " +
              transgressor.amount +
              " " +
              config.app_url;

    msg = msg.substring(0, 140); 
    return msg;
}
