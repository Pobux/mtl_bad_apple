var fetchController = require("../fetcher/fetcher")
var transgressorController = require("../transgressor/transgressor_controller")
var mail = require("../mail/mail.js");
var twitter = require("../twitter/twitter.js");
var CronJob = require("cron").CronJob;

module.exports.job =  new CronJob("00 00 * * *", function() {
        winston.info("Cron job is starting as usual.");
        fetchController.getAllTransgressor(function(err, results) { 
            if(err) {
                winston.error("Could not get transgressor. Application goes on.");
            } else {
                var transgressorList = results["contrevenants"]["contrevenant"];

                transgressorController.bulkUpsertTransgressor(transgressorList, function(err, results) {
                    if(err) {
                        winston.error("There was an error while bulk upserting.");
                        throw err;
                    }
                   
                    if(results.length > 0) {
                        winston.info("There are new transgressors (" + results.length + ").");
                        results = results.slice(0, config.cron.limit_for_extern_trigger);
                        mail.sendUpsertUpdateTo(config.mail.to, results);
                        twitter.tweetNewTransgressors(results);
                    }
                });
            } 
        });
    },

    function(){
        winston.warning("Cron job has stopped unexpectedly.");
    },
    false
);
