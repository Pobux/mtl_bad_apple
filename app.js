var express = require("express");
var path = require("path");
var favicon = require("serve-favicon");
var logger = require("morgan");
var cookieParser = require("cookie-parser");
var bodyParser = require("body-parser");
var MongoClient = require("mongodb").MongoClient;
var parseString = require("xml2js").parseString;
var yaml = require("js-yaml");
var fs = require("fs");

config = yaml.safeLoad(fs.readFileSync("config.yml", "utf8")); //global config

winston = require("winston"); //global logger
winston.add(winston.transports.File,{
        name: 'infolog',
        filename: 'logs/filelog_info.log',
        level: 'info'
    });

winston.add(winston.transports.File,{
        name: 'errorlog',
        filename: 'logs/filelog_error.log',
        level: 'error'
    });

var routes = require("./routes/index");
var transgressor = require("./routes/transgressors");

var cron = require("./lib/cron/cron")

var app = express();

//comment line if you're using nodemon'
// app.listen(3000);

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(__dirname));

app.use("/", routes);
app.use("/contrevenants", transgressor);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error("Not Found");
    err.status = 404;
    next(err);
});

app.use(function(err, req, res, next) {
    res.status(404).render("404.jade");
});

winston.info("La Mouche is starting");

//Start automatic jobs
cron.job.start();

module.exports = app;
