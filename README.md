La Mouche
=========
http://lamoucheapp.herokuapp.com

Antoine LeBel

antoine.lebel.65@gmail.com

LEBA23068603

Pour le cours de Paradigmes des échanges internet

Remis à Jacques Berger

Sommaire
========
La Mouche utilise les données ouvertes de la ville de Montréal pour répertorier la liste des restaurants qui ont eu des amendes.

Le projet a suivi la route suivante:

A1 -> A2 -> A3 -> A4 -> A5 -> A6 -> F1

B1 -> B2

En bonus : C1

L'adresse sur heroku est la suivante : http://lamoucheapp.herokuapp.com

La documentation est disponible sous l'url `/doc`.

Vous pouvez modifier les configurations de l'application sous config.yml.

Prérequis
=========
Node.js 6.5 ou plus (compatible avec 4.6)

MongoDB 3.2 ou plus

Procédure d'installation
========================
L'application doit comporter à sa racine la structure suivante :

- app.js

- config.yml

- lib/            (Contient les différents modules dédiés à l'application)

- logs/

- package.json

- public/

- raml/           (Pour la documentation)

- routes/

- tests/

- views/

*un dossier /bin est disponible pour l'utilisation de `nodemon`, il n'est pas obligatoire.

Une fois cette structure validée, tapez `npm install` ou `sudo npm install`.

Avant de démarrer l'application, vous devez vous assurer que MongoDB est installé et que le daemon (mongod) est bien activé. Il est possible de modifier les configurations d'accès à la base de données sous `config.yml`.

Ensuite démarrer l'application en tapant : `node app.js` à la racine. 

Un message devrait confirmer le démarrage `info : La Mouche is starting`.

Front-end
=========
La Mouche utilise Material Design Lite comme librairie d'affichage.

Problème(s) connu(s)
====================
- Il est possible que le package "Iconv" brise l'application. Ce cas survient quand une version de "Iconv" est incompatible avec votre version de nodejs. Il suffit de `npm uninstall iconv` et `npm install iconv`, pour que le package soit recompilé.

Regénérer la documentation
==========================
Pour tenir à jour la documentation, il suffit d'aller sous le dossier `raml/`, modifier le fichier `lamouche.raml` et taper `make`.

