var fetchController = require("../lib/fetcher/fetcher");
var transgressorController = require("../lib/transgressor/transgressor_controller")
var express = require("express");
var router = express.Router();

router.get("/", function(req, res, next) {
    fetchController.getAllTransgressor(function(err, results) { 
        if(err) {
            winston.log("Can't get transgressor.", err);
        } else {
            var transgressorList = results["contrevenants"]["contrevenant"];
            res.setHeader("Content-Type", "application/json");
            res.send(transgressorList);
        }
    });
});

module.exports = router;
