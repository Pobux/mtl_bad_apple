var express = require("express");
var router = express.Router();
var transgressorController = require("../lib/transgressor/transgressor_controller");
var raml = require("raml-parser");
var path = require("path");

router.get("/", function(req, res, next) {
    transgressorController.businessNameList(function(err, results) {
        if(err) {
            throw err;
        } 
        res.render("index", {transgressors: results});
    });
});

router.get("/doc", function(req, res, next) {

    raml.loadFile('raml/lamouche.raml').then( function(data) {
        res.sendFile(path.join(__dirname + '/../views/doc.html'));
    }, function(error) {
        winston('Error parsing raml : ' + error);
    });
})

module.exports = router;
