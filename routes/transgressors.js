var express = require("express");
var router = express.Router();
var transgressorController = require("../lib/transgressor/transgressor_controller");

router.get("/", function(req, res, next) {

    if(req.query.du > req.query.au || !("du" in req.query) || !("au" in req.query)) {
        res.setHeader("Content-Type", "application/json");
        res.status(400).send({"error" : "du option must be lower than au and both must be defined."});
    } else {

        transgressorController.dateInterval(req.query.du, req.query.au, function(err, results) {
            res.setHeader("Content-Type", "application/json");

            if(err) {
                res.status(404).send({"error" : err });
            } else {
                res.send(results);
            }
        });
    }
});

router.get("/id/:transgressorId", function(req, res, next) {
    transgressorController.findTransgressorById(req.params.transgressorId, function(err, results) {
        res.setHeader("Content-Type", "application/json");

        if(err) {
            if(err.indexOf("Wrong") != -1) {
                res.status(400).send({"error" : err });
            } else {
                res.status(404).send({"error" : err });
            }
        } else {
            res.send(results)
        }
    });
});

router.get("/name/:transgressorName", function(req, res, next) {
    transgressorController.findTransgressorByName(req.params.transgressorName, function(err, results) {
        res.setHeader("Content-Type", "application/json");

        if(err) {
            res.status(404).send({"error" : err});
        } else {
            res.send(results);
        }
    });
});

router.get("/infraction", function(req, res, next) {
    transgressorController.findTransgressorByInfractionsCount(function(err, results) {
        res.setHeader("Content-Type", "application/json");

        if(err) {
            res.status(404).send({"error" : err});
        } else {
            res.send(results);
        }
    });
});

module.exports = router;
