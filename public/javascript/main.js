//Date picker handlers
$(document).ready(function() {
    //fix on material design text-field label event triggering
    var nodeList = document.querySelectorAll('.mdl-textfield');

    Array.prototype.forEach.call(nodeList, function (elem) {
        elem.MaterialTextfield.checkDirty();
    });
});


$("#date-search").validate({
    showErrors: function(errorMap, errorList) {
        var errors = errorList.length > 0 
        if(errors) {
            _triggerError();
        } else {
            $("#date-search-submit").addClass("date-valid")
            $(".section#error").hide();
        }
    },
    onclick:true,
    onkeyup:false,
    focusCleanup:true,
    rules: {
        "date-start": {
            required: true,
            date: true
        },
        "date-end": {
            required: true,
            date: true
        }
    },
})

function _triggerError() {
    $("#date-search-submit").removeClass("date-valid")
    var msg = "Entrer des dates valides.";
    $(".section#error").html(msg);
    $(".section#error").show();
}

$("#date-search-submit").on("click", function() {
    if($(this).hasClass("date-valid")) {
        var startDate = $("#date-start").val();
        var endDate = $("#date-end").val();

        if(startDate.length != 10 || endDate.length != 10) {
            _triggerError();
            return;   
        }
        _ajaxFetchTransgressorOnDate(startDate, endDate);

    } else {
        _triggerError();
    }
});

$("#fill-in").hide();

$("#fill-in .mdl-card__menu").on("click", function() {
    $("#fill-in").hide();
});

function _ajaxFetchTransgressorOnDate(startDate, endDate) {
    $.getJSON('/contrevenants', {
        du: startDate,
        au: endDate
    }, function(data) {
        $("#fill-in").fadeIn();
        _sendInformationTable(data);
    }).fail(function() {
        _triggerError();
    });
}

function _sendInformationTable(data) {
    $("#fill-in .mdl-card__supporting-text").empty();

    var dom = [];
    for(var i = 0; i < data.length; i++) {
       dom = [
           "<div class='section__text mdl-cell mdl-cell--12-col'>",
                "<h4>" + data[i]["businessName"] + "</h4>",
                "<p>" + data[i]["address"] + " " + data[i]["city"] + "</p>",
                "<p>Propriétaire : " + data[i]["owner"] + "</p>",
                "<p>Catégorie : " + data[i]["category"] + "</p>",
                "<p>Description :</p>", 
                "<p>" + data[i]["description"] + "</p>",
                "<p>Amende : " + data[i]["amount"] + "</p>",
                "<p>Date d'infraction : " + data[i]["infraction_date"].substring(0, 10) + "</p>",
                "<p>Date de jugement : " + data[i]["judgement_date"].substring(0, 10) + "</p>",
           "</div>"
       ]; 

       $(dom.join('')).appendTo($("#fill-in .mdl-card__supporting-text"));

    }

   $("html,body").animate({scrollTop:$("#fill-in").offset().top}, 500);
}

//Name search handler
$("#name-search select").selectize({
    create: true,
    sortField: "text"
});

$("#name-search-submit").on("click", function() {
    var selectValue = _replaceWrongURLChar($("#name-search select option:selected").val());
    $.getJSON('/contrevenants/name/' + selectValue, 
        {}, 
        function(data) {
            $("#fill-in").fadeIn();
            _sendTransgressorInformation(data);
        });
});

function _replaceWrongURLChar(url) {
    var dict = {
        " " : "%20",
        "/" : "%2F"
    }

    var newUrl = "";

    for(var i = 0; i < url.length; i++) {
        if(url[i] in dict) {
            newUrl += dict[url[i]];
        } else {
            newUrl += url[i];
        } 
    }

    return newUrl;
}

function _sendTransgressorInformation(data) {
    $("#fill-in .mdl-card__supporting-text").empty();

    var dom = [];
        
    dom = [
        "<div class='section__text mdl-cell mdl-cell--12-col'>",
             "<h4>" + data["businessName"] + "</h4>",
             "<p>" + data["address"] + " " + data["city"] + "</p>",
             "<p>Propriétaire : " + data["owner"] + "</p>",
             "<p>Catégorie : " + data["category"] + "</p>",
             "<p><b>Infractions</b></p>", 
    ]; 

    for (var i = 0, len = data["infractions"].length; i < len; i++) {
        dom.push("<p>Description :</p>");
        dom.push("<p>" + data["infractions"][i]["description"] + "</p>");
        dom.push("<p>Amende : " + data["infractions"][i]["amount"] + "</p>");
        dom.push("<p>Date d'infraction : " + data["infractions"][i]["infraction_date"].substring(0, 10) + "</p>");
        dom.push("<p>Date de jugement : " + data["infractions"][i]["judgement_date"].substring(0, 10) + "</p>");
    }

    dom.push("</div>");

    $(dom.join('')).appendTo($("#fill-in .mdl-card__supporting-text"));
}
